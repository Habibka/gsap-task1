import Monster from './Monster';

export default class Animation {
  constructor() {
    this.monsters = [];
    this.fillTheMonsters();
  }

  fillTheMonsters() {
    const element = document.querySelectorAll('.monster');

    element.forEach((el) => {
      const monsterElement = new Monster(el);

      el.addEventListener('mouseenter', () => {
        element.forEach((el) => {
          el.id = 'contract';
        });
        monsterElement.expand();
      });
      el.addEventListener('mouseleave', () => {
        element.forEach((el) => {
          el.id = 'reset';
        });
        monsterElement.reset();
      });
      this.monsters.push(monsterElement);
    });
  }
}
