import gsap from 'gsap/all';

export default class Monster {

  constructor(element) {
    this.monster = element;
  }

  expand() {
    this.monster.id = 'expand';
    gsap.to('#expand', { width: '80%', duration: 0.5, id: 'expand' });
    this.contract();
  }
  contract() {
    gsap.to('#contract', { width: '4%', duration: 0.5, id: 'contract' });
  }
  reset() {
    gsap.to('#reset', { width: '16.6%', id: 'reset' });
  }
}
